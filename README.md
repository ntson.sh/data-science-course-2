# Git repository for Data science - course 2

**Path to homework files**

* Week 1: [Week 1/Homework.ipynb](https://gitlab.com/ntson.sh/data-science-course-2/blob/master/Week%201/Homework.ipynb)

Screenshot:

![Week_1 Screenshot](https://gitlab.com/ntson.sh/data-science-course-2/raw/master/Week%201/images/screenshot.jpg)
